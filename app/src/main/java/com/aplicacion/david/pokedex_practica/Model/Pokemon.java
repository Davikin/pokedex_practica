package com.aplicacion.david.pokedex_practica.Model;

import java.io.Serializable;

public class Pokemon implements Serializable {
    //IMPORTANTE: Las variables debe llamarse igual que como están en el JSON
    //Eso incluye los getters y setters

    //Para generar los getters y setters: Click derecho > Generate > Getters and setters
    private int id;
    private String name;
    private String url;

    public int getId() {
        String[] urlPartes = url.split("/");

        return Integer.parseInt(urlPartes[urlPartes.length - 1]);
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
