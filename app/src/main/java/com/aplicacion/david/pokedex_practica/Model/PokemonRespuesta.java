package com.aplicacion.david.pokedex_practica.Model;

import java.util.ArrayList;

public class PokemonRespuesta {
    //IMPORTANTE: La variable debe llamarse igual que como está en el JSON
    //Eso incluye los getters y setters

    //Para generar los getters y setters de cada variable: Click derecho > Generate > Getters and setters
    private ArrayList<Pokemon> results;

    public ArrayList<Pokemon> getResults() {
        return results;
    }

    public void setResults(ArrayList<Pokemon> results) {
        this.results = results;
    }
}
