package com.aplicacion.david.pokedex_practica;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;

import com.aplicacion.david.pokedex_practica.Model.Pokemon;
import com.aplicacion.david.pokedex_practica.Model.PokemonRespuesta;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private Retrofit retrofit;
    private static final String BASE_API_URL = "https://www.pokeapi.co/api/v2/";
    private static final String TAG = "POKEAPI";

    private RecyclerView pokemonRv;
    private PokemonAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    private boolean aptoParaCargar;

    private int offset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        pokemonRv = findViewById(R.id.pokemon_rv);
        adapter = new PokemonAdapter(this, new PokemonAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Pokemon p) {
                Intent intent = new Intent(MainActivity.this, DetallePokemonActivity.class);
                intent.putExtra("pokemon",p);
                startActivity(intent);
            }
        });

        pokemonRv.setHasFixedSize(true);

        layoutManager = new GridLayoutManager(this, 3);

        pokemonRv.setAdapter(adapter);
        pokemonRv.setLayoutManager(layoutManager);

        pokemonRv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if(dy > 0){
                    int visibleItemCount = layoutManager.getChildCount();
                    int totalItemCount = layoutManager.getItemCount();
                    int pastVisibleItems = ((GridLayoutManager)layoutManager).findFirstVisibleItemPosition();

                    if(aptoParaCargar){
                        if(visibleItemCount + pastVisibleItems >= totalItemCount){
                            Log.i(TAG, "Llegamos al final");

                            aptoParaCargar = false;
                            offset += 20;
                            obtenerDatos();
                        }
                    }
                }
            }
        });

        aptoParaCargar = true;
        offset = 0;
        obtenerDatos();
    }

    private void obtenerDatos(){
        PokeapiService service = retrofit.create(PokeapiService.class);

        //Hacemos la consulta
        Call<PokemonRespuesta> pokemonRespuestaCall = service.obtenerListaPokemon(20, offset);

        //El autocompletado de los metodos de interfaz tambien se hacen asi:
        //Empiezo escribiendo new, un espacio, y luego Ctrl+Espacio, para que aparezca el dropdown
        pokemonRespuestaCall.enqueue(new Callback<PokemonRespuesta>() {

            //Si la respuesta a la consulta nos llega
            @Override
            public void onResponse(Call<PokemonRespuesta> call, Response<PokemonRespuesta> response) {
                aptoParaCargar = true;
                //La respuesta puede sí llegar, pero con errores, por ello comprobamos si tuvo éxito
                if(response.isSuccessful()){
                    PokemonRespuesta pokemonRespuesta = response.body();
                    ArrayList<Pokemon> listaPokemon = pokemonRespuesta.getResults();

                    //Mostrar los pokemon por consola
                    for(int i = 0; i < listaPokemon.size(); i++){
                        Pokemon p = listaPokemon.get(i);
                        Log.i(TAG, "Pokemon: "+p.getName());
                    }

                    //Mostrar los pokemon en la recycler view
                    adapter.adicionarPokemon(listaPokemon);
                } else {
                    Log.e(TAG, "onResponse: " + response.errorBody());
                }
            }

            //Si falla la consulta (cuando no hay conexion a internet o ya pasó demasiado tiempo de espera, por ejemplo)
            //Aquí de plano no llegó la respuesta
            @Override
            public void onFailure(Call<PokemonRespuesta> call, Throwable t) {
                aptoParaCargar = true;
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });
    }
}
