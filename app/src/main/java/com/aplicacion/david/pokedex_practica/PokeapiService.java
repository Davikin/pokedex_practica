package com.aplicacion.david.pokedex_practica;

import com.aplicacion.david.pokedex_practica.Model.PokemonRespuesta;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface PokeapiService {
    //Recordemos que este metodo inicia sin parametros
    @GET("pokemon")
    Call<PokemonRespuesta> obtenerListaPokemon(@Query("limit") int limit, @Query("offset") int offset);
}
