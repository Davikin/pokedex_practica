package com.aplicacion.david.pokedex_practica;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.aplicacion.david.pokedex_practica.Model.Pokemon;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

public class DetallePokemonActivity extends AppCompatActivity {

    ImageView fotoIv;
    TextView nombreTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_pokemon);

        fotoIv = findViewById(R.id.foto_iv);
        nombreTv = findViewById(R.id.nombre_tv);

        Intent intentEntrante = getIntent();
        Pokemon p = (Pokemon)intentEntrante.getSerializableExtra("pokemon");

        Glide.with(this)
                .load("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + p.getId() + ".png")
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(fotoIv);

        nombreTv.setText("#"+p.getId()+" "+p.getName().toUpperCase());
    }
}
