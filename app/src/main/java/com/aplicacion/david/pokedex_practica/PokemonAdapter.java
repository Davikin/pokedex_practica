package com.aplicacion.david.pokedex_practica;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aplicacion.david.pokedex_practica.Model.Pokemon;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class PokemonAdapter extends RecyclerView.Adapter<PokemonAdapter.ViewHolder> {

    private ArrayList<Pokemon> listaPokemon;
    private Context context;
    private OnItemClickListener listener;

    public PokemonAdapter(Context context, OnItemClickListener listener) {
        this.listaPokemon = new ArrayList<>();
        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pokemon, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Pokemon p = listaPokemon.get(position);
        holder.bind(p);
    }

    @Override
    public int getItemCount() {
        return listaPokemon.size();
    }

    public void adicionarPokemon(ArrayList<Pokemon> listaPokemon) {
        this.listaPokemon.addAll(listaPokemon);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private ImageView fotoIv;
        private TextView nombreTv;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            fotoIv = itemView.findViewById(R.id.foto_image_view);
            nombreTv = itemView.findViewById(R.id.nombre_text_view);
        }

        private void bind(final Pokemon p){
            nombreTv.setText(p.getName().toUpperCase());

            Glide.with(context)
                    .load("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + p.getId() + ".png")
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(fotoIv);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(p);
                }
            });
        }
    }

    public interface OnItemClickListener{
        void onItemClick(Pokemon p);
    }
}
